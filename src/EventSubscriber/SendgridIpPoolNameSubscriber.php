<?php

namespace Drupal\sendgrid\EventSubscriber;

use Drupal\sendgrid\Event\SendgridEvents;
use Drupal\sendgrid\Event\SendgridSendEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sendgrid event subscriber for ip pool name.
 *
 * Move the options out of the main flow, so we can make them plugins after.
 */
class SendgridIpPoolNameSubscriber implements EventSubscriberInterface {

  /**
   * Sengrid send response event handler.
   *
   * @param \Drupal\sendgrid\Event\SendgridSendEvent $event
   *   Sendgrid send event.
   */
  public function onSendgridSend(SendgridSendEvent $event) {
    $ip_pool_name = $event->getConfig()->get('ip_pool_name');
    if (!empty($ip_pool_name)) {
      $email = $event->getEmail();
      try {
//        $email->setIpPoolName($ip_pool_name);
      }
      catch (\Exception $e) {
        $event->getLogger()->error(
          'Exception occurred while trying to set IP Pool Name in the email from %from. Error code @code: @message',
          [
            '%from' => $email->getFrom(),
            '@code' => $e->getCode(),
            '@message' => $e->getMessage(),
          ]
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SendgridEvents::SEND => ['onSendgridSend'],
    ];
  }

}
