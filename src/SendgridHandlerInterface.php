<?php

namespace Drupal\sendgrid;

use Drupal\key\KeyRepositoryInterface;

/**
 * The interface for Sendgrid handler service.
 */
interface SendgridHandlerInterface {

  const CONFIG_NAME = 'sendgrid.settings';

  /**
   * Sets the key repository service.
   *
   * @param \Drupal\key\KeyRepositoryInterface $repository
   *   The injected service, if it exists.
   */
  public function setKeyRepository(KeyRepositoryInterface $repository);

  /**
   * Connects to Sendgrid API and sends out the email.
   *
   * @param array $sendgridMessage
   *
   * @return bool
   *   TRUE if the mail was successfully accepted by the API, FALSE otherwise.
   */
  public function sendMail(array $sendgridMessage);

}
