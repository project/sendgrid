<?php

namespace Drupal\sendgrid\Plugin\Mail;

use Drupal\sendgrid\SendgridHandlerInterface;

/**
 * Queue the email for sending with sendgrid.
 *
 * @Mail(
 *   id = "sendgrid_queue_mail",
 *   label = @Translation("Sendgrid mailer (queued)"),
 *   description = @Translation("Sends the message using sendgrid with queue.")
 * )
 */
class SendgridQueueMail extends SendgridMail {

  /**
   * {@inheritdoc}
   */
  public function mail(array $message) {
    $sendgrid_message = $this->buildMessage($message);
    return $this->queueMessage($sendgrid_message);
  }

  /**
   * Queue a message for sending.
   *
   * @param array $message
   *   Sendgrid message array that was build and ready for sending.
   *
   * @return bool
   *   TRUE if the message was queued, otherwise FALSE.
   */
  public function queueMessage(array $message) {
    $config = $this->configFactory->get(SendgridHandlerInterface::CONFIG_NAME);

    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $this->queueFactory->get('sendgrid_send_mail');

    $item = new \stdClass();
    $item->message = $message;
    $result = $queue->createItem($item);

    if ($result !== FALSE) {
      if ($config->get('debug_mode')) {
        $this->logger->notice('Successfully queued message from %from to %to.', [
          '%from' => $message['from'],
          '%to' => $message['to'],
        ]);
      }
    }
    else {
      $this->logger->error('Unable to queue message from %from to %to.', [
        '%from' => $message['from'],
        '%to' => $message['to'],
      ]);
    }

    return !empty($result);
  }

}
