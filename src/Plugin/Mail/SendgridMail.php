<?php

namespace Drupal\sendgrid\Plugin\Mail;

use Composer\InstalledVersions;
use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Mail\MailInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Render\RendererInterface;
use Drupal\sendgrid\SendgridHandlerInterface;
use Html2Text\Html2Text;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Default sendgrid mail system plugin.
 *
 * @Mail(
 *   id = "sendgrid_mail",
 *   label = @Translation("Sendgrid mailer"),
 *   description = @Translation("Sends a message using Sendgrid.")
 * )
 */
class SendgridMail implements MailInterface, ContainerFactoryPluginInterface {

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Sendgrid handler.
   *
   * @var \Drupal\sendgrid\SendgridHandlerInterface
   */
  protected $sendgridHandler;

  /**
   * Mime type guesser service.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected $mimeTypeGuesser;

  /**
   * Constructor of a new SengridMail plugin.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue.
   * @param \Drupal\sendgrid\SendgridHandlerInterface $sendgrid_handler
   *   Sendgrid handler.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mime_type_guesser
   *   Mime type guesser.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerInterface $logger, RendererInterface $renderer, QueueFactory $queue_factory, SendgridHandlerInterface $sendgrid_handler, MimeTypeGuesserInterface $mime_type_guesser) {
    $this->configFactory = $config_factory;
    $this->logger = $logger;
    $this->renderer = $renderer;
    $this->queueFactory = $queue_factory;
    $this->sendgridHandler = $sendgrid_handler;
    $this->mimeTypeGuesser = $mime_type_guesser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory')->get('sendgrid'),
      $container->get('renderer'),
      $container->get('queue'),
      $container->get('sendgrid.mail_handler'),
      $container->get('file.mime_type.guesser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function mail(array $message) {
    $sendgrid_message = $this->buildMessage($message);
    return $this->sendgridHandler->sendMail($sendgrid_message);
  }

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    $config = $this->configFactory->get(SendgridHandlerInterface::CONFIG_NAME);
    // Join the body array into one string.
    if (is_array($message['body'])) {
      $message['body'] = implode("\n\n", $message['body']);
    }

    // If text format is specified in settings, run the message through it.
    $format = $config->get('format_filter');
    if (!empty($format)) {
      $message['body'] = check_markup($message['body'], $format, $message['langcode']);
    }

    // Skip theme formatting if the message does not support HTML.
    if (isset($message['params']['html']) && !$message['params']['html']) {
      return $message;
    }

    // Wrap body with theme function.
    if ($config->get('use_theme')) {
      $render = [
        '#theme' => $message['params']['theme'] ?? 'sendgrid',
        '#message' => $message,
      ];
      $message['body'] = $this->renderer->renderPlain($render);
    }

    return $message;
  }

  /**
   * Builds the e-mail message in preparation to be sent to sendgrid.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *   $message['params'] may contain additional parameters.
   *
   * @return array
   *   An email array formatted for Sendgrid delivery.
   */
  protected function buildMessage(array $message) {
    // Add default values to make sure those array keys exist.
    $message += [
      'body' => [],
      'params' => [],
    ];

    $site_config = $this->configFactory->get('system.site');
    $site_mail = $site_config->get('mail') ?? ini_get('sendmail_from');

    $sendgrid_message = [
      'from_email' => $site_mail ?? $message['headers']['From'] ?? '',
      'from_name' => $site_config->get('name') ?? '',
      'to' => array_map('trim', explode(',', $message['to'])),
      'subject' => $message['subject'] ?? '',
      'html' => $message['body'],
    ];

    // Remove HTML version if the message does not support HTML.
    if (isset($message['params']['html']) && !$message['params']['html']) {
      unset($sendgrid_message['html']);
    }

    // Set text version of the message.
    if (isset($message['plain'])) {
      $sendgrid_message['text'] = $message['plain'];
    }
    else {
      $converter = new Html2Text($message['body'], ['width' => 0]);
      $sendgrid_message['text'] = $converter->getText();
    }

    // Add Cc / Bcc headers.
    if (!empty($message['headers']['Cc'])) {
      $sendgrid_message['cc'] = array_map('trim', explode(',', $message['headers']['Cc']));
    }
    if (!empty($message['headers']['Bcc'])) {
      $sendgrid_message['bcc'] = array_map('trim', explode(',', $message['headers']['Bcc']));
    }

    // Add Reply-To as header according to sendgrid API.
    if (!empty($message['reply-to'])) {
      $sendgrid_message['reply-to'] = $message['reply-to'];
    }

    $sendgrid_message['headers'] = [];
    // Include custom MIME headers (for example, 'X-My-Header').
    foreach ($message['headers'] as $key => $value) {
      if (stripos($key, 'X-') === 0) {
        $sendgrid_message['headers'][(string) $key] = $value;
      }
    }

    if (!empty($message['params']['attachments'])) {
      foreach ($message['params']['attachments'] as $param_attachment) {
        $attachment = [];
        if (!empty($param_attachment['filecontent'])) {
          $attachment['filecontent'] = $param_attachment['filecontent'];
        }
        elseif (!empty($param_attachment['filepath']) && file_exists($param_attachment['filepath'])) {
          $attachment['filecontent'] = file_get_contents($param_attachment['filepath']);
          $attachment['filemime'] = $this->mimeTypeGuesser->guessMimeType($param_attachment['filepath']);
          $attachment['filename'] = pathinfo($param_attachment['filepath'], PATHINFO_BASENAME);
        }

        if (!empty($attachment['filecontent'])) {
          if (empty($attachment['filemime'])) {
            if (!empty($param_attachment['filemime'])) {
              $attachment['filemime'] = $param_attachment['filemime'];
            }
            elseif (InstalledVersions::isInstalled('ext-fileinfo')) {
              $finfo = new \finfo(FILEINFO_MIME_TYPE);
              $attachment['filemime'] = $finfo->buffer($attachment['filecontent']);
            }
          }
          if (empty($attachment['filename'])) {
            $random = new Random();
            $attachment['filename'] = $param_attachment['filename'] ?? $random->string();
          }

          $sendgrid_message['attachments'][] = $attachment;
        }
      }
    }

    return $sendgrid_message;
  }

}
