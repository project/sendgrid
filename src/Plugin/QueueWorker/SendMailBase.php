<?php

namespace Drupal\sendgrid\Plugin\QueueWorker;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\sendgrid\SendgridHandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the SendMail Queue Workers.
 */
class SendMailBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Sendgrid config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $sendgridConfig;

  /**
   * Logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Mail handler.
   *
   * @var \Drupal\sendgrid\SendgridHandlerInterface
   */
  protected $sendgridHandler;

  /**
   * SendMailBase constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImmutableConfig $settings, LoggerInterface $logger, SendgridHandlerInterface $sendgrid_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->sendgridConfig = $settings;
    $this->logger = $logger;
    $this->sendgridHandler = $sendgrid_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get(SendgridHandlerInterface::CONFIG_NAME),
      $container->get('logger.factory')->get('sendgrid'),
      $container->get('sendgrid.mail_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $result = $this->sendgridHandler->sendMail($data->message);

    if ($this->sendgridConfig->get('debug_mode')) {
      $this->logger->notice('Successfully sent message on CRON from %from to %to.',
        [
          '%from' => $data->message['from'],
          '%to' => $data->message['to'],
        ]
      );
    }

    if (!$result) {
      throw new \Exception('Sendgrid: email did not pass through API.');
    }
  }

}
