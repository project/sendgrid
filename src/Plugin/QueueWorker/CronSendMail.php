<?php

namespace Drupal\sendgrid\Plugin\QueueWorker;

/**
 * Sending mails on CRON run.
 *
 * @QueueWorker(
 *   id = "sendgrid_send_mail",
 *   title = @Translation("Sendgrid Cron Worker"),
 *   cron = {"time" = 10}
 * )
 */
class CronSendMail extends SendMailBase {}
