<?php

namespace Drupal\sendgrid\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\filter\FilterPluginManager;
use Drupal\sendgrid\SendgridHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Sendgrid configuration form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The filter plugin manager.
   *
   * @var \Drupal\filter\FilterPluginManager
   */
  protected $filterManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.filter'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, FilterPluginManager $filter_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->filterManager = $filter_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      SendgridHandlerInterface::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sendgrid_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(SendgridHandlerInterface::CONFIG_NAME);

    $form['api_key'] = [
      '#title' => $this->t('Sendgrid API Key'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => $this->t('Enter your @link.', [
        '@link' => Link::fromTextAndUrl($this->t('API key'), Url::fromUri('https://app.sendgrid.com/settings/api_keys'))->toString(),
      ]),
      '#default_value' => $config->get('api_key'),
    ];
    // Add key_select element if key module is enabled.
    if ($this->moduleHandler->moduleExists('key')) {
      $form['api_key']['#type'] = 'key_select';
      $form['api_key']['#key_filters'] = ['type' => 'authentication'];
    }

    $form['debug_mode'] = [
      '#title' => $this->t('Enable Debug Mode'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('debug_mode'),
      '#description' => $this->t('Enable to log every email and queuing.'),
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Mail options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['options']['ip_pool_name'] = [
      '#title' => $this->t('IP Pool Name'),
      '#type' => 'textfield',
      '#required' => FALSE,
      '#description' => $this->t('Enter your @link.', [
        '@link' => Link::fromTextAndUrl($this->t('IP Pool Name'), Url::fromUri('https://docs.sendgrid.com/ui/account-and-settings/ip-pools'))->toString(),
      ]),
      '#default_value' => $config->get('ip_pool_name'),
    ];

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $options = [
      '' => $this->t('None'),
    ];
    $filter_formats = filter_formats();
    foreach ($filter_formats as $filter_format_id => $filter_format) {
      $options[$filter_format_id] = $filter_format->label();
    }

    // Add additional description text if there is a recommended filter plugin.
    // To be sure we are using the correct plugin name, let's use the plugin
    // definition.
    $recommendation = !$this->filterManager->hasDefinition('filter_autop') ? ''
      : $this->t('Recommended format filters: @filter.', ['@filter' => $this->filterManager->getDefinition('filter_autop')['title'] ?? '']);

    $form['advanced_settings']['format_filter'] = [
      '#title' => $this->t('Format filter'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $config->get('format_filter'),
      '#description' => $this->t('@text_format to use to render the message. @recommendation', [
        '@text_format' => Link::fromTextAndUrl($this->t('Text format'), Url::fromRoute('filter.admin_overview'))->toString(),
        '@recommendation' => $recommendation,
      ]),
    ];
    $form['advanced_settings']['use_theme'] = [
      '#title' => $this->t('Use theme'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('use_theme'),
      '#description' => $this->t('Enable to pass the message through a theme function. Default "sendgrid" or pass one with $message["params"]["theme"].'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_keys = [
      'api_key',
      'debug_mode',
      'ip_pool_name',
      'format_filter',
      'use_theme',
    ];

    $sendgrid_config = $this->config(SendgridHandlerInterface::CONFIG_NAME);
    foreach ($config_keys as $config_key) {
      if ($form_state->hasValue($config_key)) {
        $sendgrid_config->set($config_key, $form_state->getValue($config_key));
      }
    }
    $sendgrid_config->save();

    $this->messenger()->addMessage($this->t('The configuration options have been saved.'));
  }

}
