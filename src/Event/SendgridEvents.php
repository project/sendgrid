<?php

namespace Drupal\sendgrid\Event;

/**
 * Defines Sendgrid Events constants.
 */
final class SendgridEvents {

  /**
   * Dispatched event just before sending the email request to Sendgrid.
   *
   * @Event
   *
   * @see \Drupal\sendgrid\Event\SendgridSendEvent
   *
   * @var string
   */
  const SEND = 'sendgrid.send';

}
