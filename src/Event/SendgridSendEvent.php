<?php

namespace Drupal\sendgrid\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Config\ImmutableConfig;
use Psr\Log\LoggerInterface;
use SendGrid\Mail\Mail;

/**
 * Sendgrid events.
 */
class SendgridSendEvent extends Event {

  /**
   * The email object.
   *
   * @var \SendGrid\Mail\Mail
   */
  protected $email;

  /**
   * The Sendgrid object.
   *
   * @var \SendGrid
   */
  protected $sendgrid;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * SendgridSendEvent constructor.
   *
   * @param \SendGrid\Mail\Mail $email
   *   Email to be altered.
   * @param \SendGrid $sendgrid
   *   Sendgrid object.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(Mail $email, \SendGrid $sendgrid, ImmutableConfig $config, LoggerInterface $logger) {
    $this->sendgrid = $sendgrid;
    $this->email = $email;
    $this->config = $config;
    $this->logger = $logger;
  }

  /**
   * Gets the email.
   *
   * @return \SendGrid\Mail\Mail
   *   The email object.
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Gets the Sendgrid object.
   *
   * @return \SendGrid
   *   The sendgrid object.
   */
  public function getSendgrid() {
    return $this->sendgrid;
  }

  /**
   * Gets the configuration object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The config object.
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Gets the logger instance.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  public function getLogger() {
    return $this->logger;
  }

}
