<?php

namespace Drupal\sendgrid;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\sendgrid\Event\SendgridEvents;
use Drupal\sendgrid\Event\SendgridSendEvent;
use Psr\Log\LoggerInterface;
use SendGrid\Mail\Attachment;
use SendGrid\Mail\Mail;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Mail handler to send out an email message array to the sendgrid API.
 */
class SendgridHandler implements SendgridHandlerInterface {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The key repository service.
   *
   * @var \Drupal\key\KeyRepositoryInterface|null
   */
  protected $keyRepository;

  /**
   * Constructs a new \Drupal\sendgrid\MailHandler object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The uuid service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Used for dispatching sendgrid events.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerInterface $logger, MessengerInterface $messenger, EmailValidatorInterface $email_validator, UuidInterface $uuid_service, EventDispatcherInterface $event_dispatcher) {
    $this->config = $config_factory->get(SendgridHandlerInterface::CONFIG_NAME);
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->emailValidator = $email_validator;
    $this->uuidService = $uuid_service;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function sendMail(array $message) {
    try {
      if (empty($message['from_email']) || empty($message['to'])) {
        $this->logger->error('From or to addresses were empty when trying to send a message.');
        return FALSE;
      }

      $email = new Mail();
      $email->setFrom($message['from_email'], $message['from_name']);
      $email->setSubject($message['subject']);
      $message['to'] = array_filter($message['to'], fn ($to) => $this->emailValidator->isValid($to));
      if (empty($message['to'])) {
        $this->logger->error('None of the "to" addresses were valid in the email from %from to %to. Error code @code: @message',
          [
            '%from' => $message['from'],
            '%to' => $this->getRecipients($message),
          ]
        );
        return FALSE;
      }

      foreach ($message['to'] as $to) {
        $email->addTo($to);
      }

      if (!empty($message['text'])) {
        $email->addContent('text/plain', (string) $message['text']);
      }
      if (!empty($message['html'])) {
        $email->addContent('text/html', (string) $message['html']);
      }
      if (!empty($message['cc'])) {
        foreach ($message['cc'] as $cc) {
          $email->addCc($cc);
        }
      }
      if (!empty($message['bcc'])) {
        foreach ($message['bcc'] as $bcc) {
          $email->addCc($bcc);
        }
      }
      if (!empty($message['reply-to'])) {
        $email->setReplyTo($message['reply-to']);
      }
      if (!empty($message['headers'])) {
        foreach ($message['headers'] as $key => $value) {
          $email->addHeader($key, $value);
        }
      }
      if (!empty($message['attachments'])) {
        foreach ($message['attachments'] as $attachment) {
          if (!empty($attachment['filecontent'])) {
            $sendgrid_attachment = new Attachment();
            $sendgrid_attachment->setContent($attachment['filecontent']);
            $sendgrid_attachment->setType($attachment['filemime']);
            $sendgrid_attachment->setFilename($attachment['filename']);
            $sendgrid_attachment->setDisposition('attachment');
            $sendgrid_attachment->setContentID($this->uuidService->generate());
            $email->addAttachment($sendgrid_attachment);
          }
        }
      }
      $sendgrid = new \SendGrid($this->getApiKey());

      // Allow other modules to alter the email.
      $event = new SendgridSendEvent($email, $sendgrid, $this->config, $this->logger);
      $this->eventDispatcher->dispatch($event, SendgridEvents::SEND);
      $email = $event->getEmail();

      $response = $sendgrid->send($email);
      if ($response->statusCode() >= 200 && $response->statusCode() <= 300) {
        // Debug mode: log all messages.
        if ($this->config->get('debug_mode')) {
          $this->logger->notice('Successfully sent message from %from to %to. %message.',
            [
              '%from' => $message['from_email'],
              '%to' => $this->getRecipients($message),
              '%message' => $response->body(),
            ]
          );
        }
        return TRUE;
      }
      else {
        $this->logger->error('Exception occurred while trying to send email from %from to %to. Error code @code: @message',
          [
            '%from' => $message['from_email'],
            '%to' => $this->getRecipients($message),
            '@code' => $response->statusCode(),
            '@message' => $response->body(),
          ]
        );
        return FALSE;
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Exception occurred while trying to send email from %from to %to. Error code @code: @message',
        [
          '%from' => $message['from_email'],
          '%to' => $this->getRecipients($message),
          '@code' => $e->getCode(),
          '@message' => $e->getMessage(),
        ]
      );
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setKeyRepository(KeyRepositoryInterface $repository = NULL) {
    $this->keyRepository = $repository;
  }

  /**
   * Returns a list of recipients for error/debug log message.
   *
   * @param array $message
   *   A message array, as described in
   *   https://documentation.sendgrid.com/en/latest/api-sending.html#sending.
   *
   * @return string
   *   Recipients list in the following format:
   *   user@test.com, user1@test.com; cc: user2@test.com; bcc: user3@test.com.
   */
  protected function getRecipients(array $message) {
    $recipients = is_array($message['to']) ? implode(', ', $message['to']) : $message['to'];

    // Add all recipients (including 'cc' and 'bcc').
    foreach (['cc', 'bcc'] as $parameter) {
      if (!empty($message[$parameter])) {
        $extra_recipients = is_array($message[$parameter]) ? implode(', ', $message[$parameter]) : $message[$parameter];
        $recipients .= "; {$parameter}: $extra_recipients";
      }
    }
    return $recipients;
  }

  /**
   * Returns the Sendgrid API Key
   *
   * @return string
   *   The Sendgrid API key.
   */
  protected function getApiKey(): string {
    $api_key = $this->config->get('api_key');

    // If the key repository is not set, return the key as is, asuming it is the
    // actual key.
    if (!$this->keyRepository instanceof KeyRepositoryInterface) {
      return $api_key;
    }

    $store = $this->keyRepository->getKey($api_key);
    if (empty($store)) {
      return $api_key;
    }

    $store_key = $store->getKeyValue();
    if (empty($store_key)) {
      return $api_key;
    }

    return $store_key;
  }

}
