<?php

namespace Drupal\Tests\sendgrid\Functional;

use Drupal\key\Entity\Key;

/**
 * Tests settings form with key module installed.
 *
 * @group sendgrid
 */
class SettingsFormKeyTest extends SettingsFormTest {

  /**
   * {@inheritdoc}
   */
  protected $apiKeySelector = 'select[name="api_key"]';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['key'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $key = Key::create([
      'id' => 'sendgrid_api_key',
      'label' => 'Sendgrid API Key',
      'key_type' => 'authentication',
      'key_type_settings' => [],
      'key_provider' => 'config',
      'key_provider_settings' => [
        'key_value' => '123456789-abc-from-key',
      ],
      'key_input' => 'text_field',
      'key_test_multi_settings' => [],
    ]);
    $key->save();
    $this->settingValues['api_key'] = 'sendgrid_api_key';
  }

}
