<?php

namespace Drupal\Tests\sendgrid\Functional;

use Drupal\Core\Url;
use Drupal\sendgrid\SendgridHandlerInterface;

/**
 * Tests settings form.
 *
 * @group sendgrid
 */
class SettingsFormTest extends SendgridFunctionalTestBase {

  /**
   * Admin user for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The selector for the API key field.
   */
  protected $apiKeySelector = 'input[name="api_key"]';

  /**
   * Sendgrid configuration values.
   *
   * @var array
   */
  protected $settingValues = [
    'api_key' => '123456789-abc',
    'debug_mode' => TRUE,
    'ip_pool_name' => '',
    'use_theme' => TRUE,
    'format_filter' => '',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser($this->permissions);
  }

  /**
   * Tests settings form submit.
   */
  public function testSettingsFormSubmit() {
    $this->drupalLogin($this->adminUser);

    $this->drupalGet(Url::fromRoute('sendgrid.settings_form'));

    // Make sure that "API Key" field is visible and required.
    $api_key_field = $this->assertSession()
      ->elementExists('css', $this->apiKeySelector);
    $this->assertTrue($api_key_field->hasAttribute('required'));

    // Save additional parameters. Check that all fields available on the form.
    $this->submitSettingsForm($this->settingValues, 'The configuration options have been saved.');

    // Rebuild config values after form submit.
    $this->sendgridConfig = $this->config(SendgridHandlerInterface::CONFIG_NAME);

    // Test that all field values are stored in configuration.
    foreach ($this->settingValues as $field_name => $field_value) {
      $this->assertEquals($field_value, $this->sendgridConfig->get($field_name));
    }
  }

  /**
   * Submits sendgrid settings form with given values and checks status message.
   */
  private function submitSettingsForm(array $values, $result_message) {
    foreach ($values as $field_name => $field_value) {
      $this->getSession()->getPage()->fillField($field_name, $field_value);
    }
    $this->getSession()->getPage()->pressButton('Save configuration');
    $this->assertSession()->pageTextContains($result_message);
  }

}
