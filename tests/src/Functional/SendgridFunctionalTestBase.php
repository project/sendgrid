<?php

namespace Drupal\Tests\sendgrid\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\sendgrid\SendgridHandlerInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Base test class for sendgrid functional tests.
 */
abstract class SendgridFunctionalTestBase extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sendgrid'];

  /**
   * Permissions required by the user to perform the tests.
   *
   * @var array
   */
  protected $permissions = ['administer sendgrid'];

  /**
   * An editable config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $sendgridConfig;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->sendgridConfig = $this->config(SendgridHandlerInterface::CONFIG_NAME);
  }

}
