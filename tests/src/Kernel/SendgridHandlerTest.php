<?php

namespace Drupal\Tests\sendgrid\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\key\Entity\Key;
use Drupal\key\KeyRepositoryInterface;
use Drupal\sendgrid\SendgridHandler;
use Drupal\sendgrid\SendgridHandlerInterface;

/**
 * Test the Sendgrid mail handler.
 *
 * @coversDefaultClass \Drupal\sendgrid\SendgridHandler
 * @group sendgrid
 */
class SendgridHandlerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'key',
    'sendgrid',
  ];

  /**
   * The Sendgrid mail handler.
   *
   * @var \Drupal\sendgrid\SendgridHandlerInterface
   */
  protected $handler;

  /**
   * The key entity with the sendgrid API key.
   *
   * @var \Drupal\key\Entity\Key
   */
  protected $key;

  /**
   * Sendgrid configuration values.
   *
   * @var array
   */
  protected $settingValues = [
    'api_key' => '123456789-abc',
    'debug_mode' => TRUE,
    'ip_pool_name' => '',
    'use_theme' => TRUE,
    'format_filter' => '',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['sendgrid']);
    $this->installEntitySchema('key');

    $this->key = Key::create([
      'id' => 'sendgrid_api_key',
      'label' => 'Sendgrid API Key',
      'key_type' => 'authentication',
      'key_type_settings' => [],
      'key_provider' => 'config',
      'key_provider_settings' => [
        'key_value' => '123456789-abc-from-key',
      ],
      'key_input' => 'text_field',
      'key_test_multi_settings' => [],
    ]);
    $this->key->save();

    $this->container->get('config.factory')
      ->getEditable('sendgrid.settings')
      ->setData($this->settingValues)
      ->save();

    $this->handler = $this->container->get('sendgrid.mail_handler');
  }

  /**
   * API Key is returned as is when key repository is not set.
   *
   * @covers ::getApiKey
   */
  public function testGetApiKeyAsString() {
    $reflected_method = new \ReflectionMethod(SendgridHandler::class, 'getApiKey');
    $reflected_property = new \ReflectionProperty(SendgridHandler::class, 'keyRepository');

    $this->assertInstanceOf(SendgridHandlerInterface::class, $this->handler);
    $this->handler->setKeyRepository(NULL);
    $this->assertEmpty($reflected_property->getValue($this->handler));
    $this->assertEquals($this->settingValues['api_key'], $reflected_method->invoke($this->handler));
  }

  /**
   * API Key is returned as is when key repository is set.
   *
   * This tests ensures that the API key is returned as is when the key
   * repository is set but there is no key configured.
   *
   * @covers ::getApiKey
   */
  public function testGetApiKeyAsStringWithKey() {
    $reflected_method = new \ReflectionMethod(SendgridHandler::class, 'getApiKey');
    $reflected_property = new \ReflectionProperty(SendgridHandler::class, 'keyRepository');

    $this->assertInstanceOf(SendgridHandlerInterface::class, $this->handler);
    $this->handler->setKeyRepository($this->container->get('key.repository'));
    $this->assertInstanceOf(KeyRepositoryInterface::class, $reflected_property->getValue($this->handler));
    $this->assertEquals($this->settingValues['api_key'], $reflected_method->invoke($this->handler));
  }

  /**
   * API Key is returned as is when key repository is set and key is configured.
   *
   * This tests ensures that the API key is returned as is when the key
   * repository is sets and there is an existing key configured.
   *
   * @covers ::getApiKey
   */
  public function testGetApiKeyWithKeyConfigured() {
    // Configure the key as the API key in the sendgrid settings.
    $settings = $this->settingValues;
    $settings['api_key'] = $this->key->id();
    $this->container->get('config.factory')
      ->getEditable('sendgrid.settings')
      ->setData($settings)
      ->save();

    $reflected_method = new \ReflectionMethod(SendgridHandler::class, 'getApiKey');
    $reflected_property = new \ReflectionProperty(SendgridHandler::class, 'keyRepository');

    $this->assertInstanceOf(SendgridHandlerInterface::class, $this->handler);
    // Ensure the key repository is set on the handler.
    $this->handler->setKeyRepository($this->container->get('key.repository'));

    // Ensure the key value is returned as the API key.
    $this->assertInstanceOf(KeyRepositoryInterface::class, $reflected_property->getValue($this->handler));
    $this->assertEquals($this->key->getKeyValue(), $reflected_method->invoke($this->handler));
  }

}
